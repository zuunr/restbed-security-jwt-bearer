/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authentication;

import reactor.core.publisher.Mono;

/**
 * <p>The JwtSecretProvider is responsible for returning a jwt secret
 * in its byte[] format.</p>
 * 
 * <p>The interface supports reactive {@link Mono} types.</p>
 *
 * @author Mikael Ahlberg
 */
public interface JwtSecretProvider {
    
    /**
     * <p>Returns a jwt secret (string) in byte[] format given the
     * provided apiName.</p>
     * 
     * @param apiName is the api name of which this secret will be used for
     * @return a jwt secret string in byte[] format
     */
    Mono<byte[]> getJwtSecret(String apiName);
    
}
