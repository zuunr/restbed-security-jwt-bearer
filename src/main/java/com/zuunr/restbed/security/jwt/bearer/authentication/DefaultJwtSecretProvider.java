/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authentication;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import reactor.core.publisher.Mono;

/**
 * <p>The DefaultJwtSecretProvider returns the configured
 * zuunr.security.jwt.bearer.jwtSecret at all times regardless
 * of the provided apiName.</p>
 *
 * @see JwtSecretProvider
 *
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "zuunr.security.jwt.bearer")
@Validated
public class DefaultJwtSecretProvider implements JwtSecretProvider {
    
    @NotEmpty(message = "You must set the jwt secret")
    @Size(min = 64, message = "The jwt secret key must be at least 64 chars (512 bits)")
    private final String jwtSecret;
    
    @ConstructorBinding
    public DefaultJwtSecretProvider(String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    @Override
    public Mono<byte[]> getJwtSecret(String apiName) {
        return Mono.just(jwtSecret.getBytes());
    }
}
