/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authentication;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class DefaultJwtSecretProviderTest {
    
    @Test
    void givenAnyApiNameShouldReturnSecret() {
        final String jwtSecret = "mySecret";
        DefaultJwtSecretProvider defaultJwtSecretProvider = new DefaultJwtSecretProvider(jwtSecret);
        
        assertArrayEquals(jwtSecret.getBytes(), defaultJwtSecretProvider.getJwtSecret(null).block());
        assertArrayEquals(jwtSecret.getBytes(), defaultJwtSecretProvider.getJwtSecret("someApi").block());
        assertArrayEquals(jwtSecret.getBytes(), defaultJwtSecretProvider.getJwtSecret("anotherApi").block());
    }
}
