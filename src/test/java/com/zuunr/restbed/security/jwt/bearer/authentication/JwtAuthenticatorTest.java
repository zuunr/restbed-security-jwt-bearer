/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authentication;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.Key;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import reactor.core.publisher.Mono;

class JwtAuthenticatorTest {

    private final String JWT_SECRET = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";

    @Test
    void givenTokenShouldDecodeIt() {
        final String subject = "laura_andersson";
        final JsonArray roles = JsonArray.EMPTY.add("ADMIN");
        final String context = "sweden.stockholm";
        final long timeToLiveInSeconds = 3600;
        
        final String token = createToken(subject, roles, context, timeToLiveInSeconds);

        JwtAuthenticator jwtAuthenticator = new JwtAuthenticator(o -> Mono.just(JWT_SECRET.getBytes()), new JsonObjectFactory());
        JsonObject claims = jwtAuthenticator.getClaims("apiName", token).block();

        assertEquals(subject, claims.get("sub").as(String.class));
        assertEquals(roles.asJson(), claims.get("roles").asJson());
        assertEquals(context, claims.get("context").as(String.class));

        Instant now = OffsetDateTime.now(ZoneOffset.UTC).toInstant();
        assertTrue(now.isBefore(Instant.ofEpochSecond(claims.get("exp").asLong())));
    }
    
    @Test
    void givenInvalidOrMissingTokenShouldThrowException() {
        JwtAuthenticator jwtAuthenticator = new JwtAuthenticator(o -> Mono.just(JWT_SECRET.getBytes()), new JsonObjectFactory());
        assertThrows(AuthenticationException.class, () -> jwtAuthenticator.getClaims("apiName", "faultyToken").block());
    }
    
    private String createToken(String subject, JsonArray roles, String context, long timeToLiveInSeconds) {
        Key secret = Keys.hmacShaKeyFor(JWT_SECRET.getBytes());

        Date expiration = Date.from(OffsetDateTime.now(ZoneOffset.UTC)
                .plusSeconds(timeToLiveInSeconds)
                .toInstant());

        return Jwts.builder()
                .setSubject(subject)
                .setExpiration(expiration)
                .claim("roles", roles.asMapsAndLists())
                .claim("context", context)
                .signWith(secret, SignatureAlgorithm.HS512)
                .compact();
    }
}
