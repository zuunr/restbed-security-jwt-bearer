/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.authorization.claims.ClaimsProvider;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.security.jwt.bearer.authentication.JwtAuthenticator;
import com.zuunr.restbed.security.jwt.bearer.authorization.claims.UnauthenticatedClaimsProvider;

import reactor.core.publisher.Mono;

/**
 * <p>Jwt Bearer implementation of the {@link ClaimsProvider} interface.
 * The JwtClaimsProvider authenticates and extracts the claims
 * from the provided jwt bearer token.</p>
 * 
 * @see ClaimsProvider
 *
 * @author Mikael Ahlberg
 */
@Component
public class JwtClaimsProvider implements ClaimsProvider {
    
    private final Logger logger = LoggerFactory.getLogger(JwtClaimsProvider.class);
    
    private AuthorizationBearerProvider authorizationBearerProvider;
    private JwtAuthenticator jwtAuthenticator;
    private UnauthenticatedClaimsProvider unauthenticatedClaimsProvider;
    
    @Autowired
    public JwtClaimsProvider(
            AuthorizationBearerProvider authorizationBearerProvider,
            JwtAuthenticator jwtAuthenticator,
            UnauthenticatedClaimsProvider unauthenticatedClaimsProvider) {
        this.authorizationBearerProvider = authorizationBearerProvider;
        this.jwtAuthenticator = jwtAuthenticator;
        this.unauthenticatedClaimsProvider = unauthenticatedClaimsProvider;
    }

    @Override
    public Mono<JsonObject> getClaims(Request<JsonObjectWrapper> request) {
        return Mono.just(request)
                .flatMap(o -> Mono.justOrEmpty(authorizationBearerProvider.getBearer(request)))
                .flatMap(o -> jwtAuthenticator.getClaims(request.getApiUriInfo().apiName(), o))
                .onErrorResume(error -> {
                    logger.error("Authentication failure", error);
                    return Mono.just(JsonObject.EMPTY);
                })
                .switchIfEmpty(unauthenticatedClaimsProvider.getUnauthenticatedClaims(request.getApiUriInfo().apiName()));
    }
}
