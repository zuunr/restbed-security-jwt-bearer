# restbed-security-jwt-bearer

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-security-jwt-bearer.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-security-jwt-bearer)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-security-jwt-bearer/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-security-jwt-bearer)

This is the jwt bearer implementation for Restbed modules. It implements the claim provider interface in the core project, providing support for basic bearer jwt token authentication and authorization.

## Supported tags

* [`1.0.0.M5-b86185d`, (*b86185d/pom.xml*)](https://bitbucket.org/zuunr/restbed-security-jwt-bearer/src/b86185d/pom.xml)

## Usage

To use this module, make sure this project's maven artifact is added as a dependency in your module, replacing 1.0.0-abcdefg with a supported tag:

```xml
<dependency>
    <groupId>com.zuunr.restbed</groupId>
    <artifactId>restbed-security-jwt-bearer</artifactId>
    <version>1.0.0-abcdefg</version>
</dependency>
```

## Configuration

The module requires additional configuration, see src/test/resources/application.yml for a sample.

Remember to use environment variables for security properties instead of the application.yml file in a non-test environment.
