/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;

import io.jsonwebtoken.Jwts;
import reactor.core.publisher.Mono;

/**
 * <p>The JwtAuthenticator is responsible for authenticating
 * provided jwt tokens.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class JwtAuthenticator {

    private JwtSecretProvider jwtSecretProvider;
    private JsonObjectFactory jsonObjectFactory;

    @Autowired
    public JwtAuthenticator(
            JwtSecretProvider jwtSecretProvider,
            JsonObjectFactory jsonObjectFactory) {
        this.jwtSecretProvider = jwtSecretProvider;
        this.jsonObjectFactory = jsonObjectFactory;
    }

    /**
     * <p>Validates the jwt token and returns its claims as a {@link JsonObject}.</p>
     * 
     * <p>If the token is invalid an {@link AuthenticationException} will be thrown.</p>
     * 
     * @param apiName is the requests api name
     * @param token is the token to parse
     * @return a json object containing the token's claims
     */
    public Mono<JsonObject> getClaims(String apiName, String token) {
        return Mono.just(apiName)
                .flatMap(jwtSecretProvider::getJwtSecret)
                .map(secret -> Jwts.parserBuilder().setSigningKey(secret).build().parseClaimsJws(token).getBody())
                .map(jsonObjectFactory::createJsonObject)
                .onErrorMap(error -> new AuthenticationException("No valid token provided", error));
    }
}
