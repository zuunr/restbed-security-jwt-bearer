/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authorization;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.security.jwt.bearer.authentication.AuthenticationException;
import com.zuunr.restbed.security.jwt.bearer.authentication.JwtAuthenticator;
import com.zuunr.restbed.security.jwt.bearer.authorization.claims.UnauthenticatedClaimsProvider;

import reactor.core.publisher.Mono;

class JwtClaimsProviderTest {
    
    @Test
    void givenTokenShouldExtractClaims() {
        final String bearerToken = "myToken";
        final JsonObject expectedClaims = JsonObject.EMPTY
                .put("ctx", "Laura Andersson")
                .put("roles", JsonArray.EMPTY
                        .add("ADMIN"));
        
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearer " + bearerToken)));
        
        JwtClaimsProvider jwtClaimsProvider = new JwtClaimsProvider(new AuthorizationBearerProvider(), new JwtAuthenticator(null, null) {
            @Override
            public Mono<JsonObject> getClaims(String apiName, String token) {
                assertEquals(bearerToken, token);
                return Mono.just(expectedClaims);
            }
        }, new UnauthenticatedClaimsProvider(null, null));
        
        JsonObject claims = jwtClaimsProvider.getClaims(request).block();
        
        assertEquals(expectedClaims, claims);
    }
    
    @Test
    void givenFaultyTokenShouldReturnEmptyClaims() {
        String bearerToken = "faultyToken";
        
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearer " + bearerToken)));
        
        JwtClaimsProvider jwtClaimsProvider = new JwtClaimsProvider(new AuthorizationBearerProvider(), new JwtAuthenticator(null, null) {
            @Override
            public Mono<JsonObject> getClaims(String apiName, String token) {
                throw new AuthenticationException("No valid token provided", null);
            }
        }, new UnauthenticatedClaimsProvider(null, null));
        
        JsonObject claims = jwtClaimsProvider.getClaims(request).block();
        
        assertEquals(JsonObject.EMPTY, claims);
    }
    
    @Test
    void givenFaultyAuthorizationHeaderShouldReturnEmptyClaims() {
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bear")));
        
        JwtClaimsProvider jwtClaimsProvider = new JwtClaimsProvider(new AuthorizationBearerProvider(), new JwtAuthenticator(null, null) {
            @Override
            public Mono<JsonObject> getClaims(String apiName, String token) {
                fail("Should not try to get claims when no token provided");
                return null;
            }
        }, new UnauthenticatedClaimsProvider(null, null));
        
        JsonObject claims = jwtClaimsProvider.getClaims(request).block();
        
        assertEquals(JsonObject.EMPTY, claims);
    }
    
    @Test
    void givenMissingTokenShouldReturnDefaultClaims() {        
        final JsonObject expectedClaims = JsonObject.EMPTY
                .put("ctx", "anonymous")
                .put("roles", JsonArray.EMPTY
                        .add("ANONYMOUS"));
        
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY);
        
        JwtClaimsProvider jwtClaimsProvider = new JwtClaimsProvider(new AuthorizationBearerProvider(), new JwtAuthenticator(null, null) {
            @Override
            public Mono<JsonObject> getClaims(String apiName, String token) {
                fail("Should not try to get claims when no token provided");
                return null;
            }
        }, new UnauthenticatedClaimsProvider(null, null) {
            @Override
            public Mono<JsonObject> getUnauthenticatedClaims(String apiName) {
                return Mono.just(expectedClaims);
            }
        });
        
        JsonObject claims = jwtClaimsProvider.getClaims(request).block();
        
        assertEquals(expectedClaims, claims);
    }
}
