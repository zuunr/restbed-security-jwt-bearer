/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.jwt.bearer.authorization;

import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Request;

/**
 * <p>The AuthorizationBearerProvider is responsible for extracting
 * the Bearer token from the provided request.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class AuthorizationBearerProvider {

    /**
     * <p>Returns the token part in the Authorization header, removing
     * the initial 'Bearer ' part.</p>
     * 
     * <p>If the authorization header is malformed, an {@link BearerException}
     * will be thrown.</p>
     * 
     * @param request is the request containing the request headers
     * @return a string containing the token, or null if header is not set
     */
    public String getBearer(Request<JsonObjectWrapper> request) {
        return Optional.ofNullable(request.getHeaders().get("authorization"))
                .map(o -> extractBearer(o.get(0).getValue(String.class)))
                .orElse(null);
    }
    
    private String extractBearer(String authorizationHeader) {
        if (StringUtils.startsWithIgnoreCase(authorizationHeader, "bearer ")) {
            return authorizationHeader.substring(7);
        }
        
        throw new BearerException("Malformed bearer provided");
    }
}
